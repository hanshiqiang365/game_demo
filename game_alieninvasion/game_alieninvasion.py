﻿#author: hanshiqiang365 （微信公众号：韩思工作室）

import os
import sys
import random
import pygame

WIDTH = 800
HEIGHT = 600
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GREEN = (50, 250, 5)
RED = (255, 0, 0)
FPS = 60

def showText(screen, text, color, font, x, y):
	text = font.render(text, True, color)
	screen.blit(text, (x, y))

def showLife(screen, num_life, color):
	cell = [2, 2]
	num_cols = 15
	filled_cells = [7,21,22,23,36,37,38,46,47,48,49,50,51,52,53,54,55,56,57,58,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119]
	for i in range(num_life):
		position = [750-35*i, 8]
		for i in range(0, len(filled_cells)):
			y = filled_cells[i] // num_cols
			x = filled_cells[i] % num_cols
			rect = [x * cell[0] + position[0], y * cell[1] + position[1], cell[0], cell[1]]
			pygame.draw.rect(screen, color, rect)

def endInterface(screen, color, is_win):
	screen.fill(color)
	clock = pygame.time.Clock()
	if is_win:
		text = 'YOU WIN!'
	else:
		text = 'GAME OVER'
	font = pygame.font.SysFont('arial', 30)
	text_render = font.render(text, 1, (255, 255, 255))
	while True:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				sys.exit()
			if (event.type == pygame.KEYDOWN) or (event.type == pygame.MOUSEBUTTONDOWN):
				return
		screen.blit(text_render, (350, 300))
		clock.tick(60)
		pygame.display.update()

class aircraftSprite(pygame.sprite.Sprite):
	def __init__(self, color, bullet_color, **kwargs):
		pygame.sprite.Sprite.__init__(self)
		self.num_life = 3
		self.max_num_life = 5
		self.cell = [3, 3]
		self.num_cols = 15
		self.num_rows = 8
		self.rect = pygame.Rect(0, 550, self.cell[0]*self.num_cols, self.cell[0]*self.num_rows)
		self.filled_cells = [7,21,22,23,36,37,38,46,47,48,49,50,51,52,53,54,55,56,57,58,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119]
		self.color = color
		self.bullet_color = bullet_color
		self.is_cooling = False
		self.init_count = 35
		self.cooling_count = self.init_count
		self.score = 0
		self.old_score = -1
		self.resetBoom()

	def shot(self):
		if self.is_cooling:
			return None
		self.is_cooling = True
		self.cooling_count = self.init_count
		return myBulletSprite(self.rect.x + self.rect.width // 2, self.rect.y, self.bullet_color)

	def draw(self, screen):
		for i in range(0, len(self.filled_cells)):
			y = self.filled_cells[i] // self.num_cols
			x = self.filled_cells[i] % self.num_cols
			rect = [x * self.cell[0] + self.rect[0], y * self.cell[1] + self.rect[1], self.cell[0], self.cell[1]]
			pygame.draw.rect(screen, self.color, rect)

	def update(self, WIDTH):
		x = pygame.mouse.get_pos()[0] - (self.rect.width // 2)
		if x < 0:
			x = pygame.mouse.get_pos()[0]
		elif x > WIDTH - self.rect.width:
			x = WIDTH - self.rect.width
		self.rect.x = x
		if self.is_cooling:
			self.cooling_count -= 1
			if self.cooling_count == 0:
				self.is_cooling = False

	def boom(self, screen):
		self.boomed_rect.x = self.rect.x
		self.boomed_rect.y = self.rect.y
		self.boomed_count += 1
		if self.boomed_count % 1 == 0:
			self.boomed_frame += 1
			for i in range(0, len(self.boomed_filled_cells)):
				y = self.boomed_filled_cells[i] // self.boomed_num_cols
				x = self.boomed_filled_cells[i] % self.boomed_num_cols
				rect = [x * self.boomed_cell[0] + self.boomed_rect[0], y * self.boomed_cell[1] + self.boomed_rect[1], self.boomed_cell[0], self.boomed_cell[1]]
				pygame.draw.rect(screen, self.color, rect)
		if self.boomed_frame > 4:
			return True
		else:
			return False
		
	def resetBoom(self):
		self.one_dead = False
		self.boomed_filled_cells = [3,7,12,15,17,20,24,30,36,40,44,45,53,54,58,62,68,74,78,81,83,86,91,95]
		self.boomed_cell = [3, 3]
		self.boomed_num_cols = 11
		self.boomed_num_rows = 9
		self.boomed_rect = pygame.Rect(0, 0, self.boomed_num_cols*self.boomed_cell[0], self.boomed_num_rows*self.boomed_cell[1])
		self.boomed_count = 0
		self.boomed_frame = 0

class ufoSprite(pygame.sprite.Sprite):
	def __init__(self, color, **kwargs):
		pygame.sprite.Sprite.__init__(self)
		self.reward = 200
		self.color = color
		self.reset()

	def draw(self, screen):
		if self.is_dead:
			return None
		for i in range(0, len(self.filled_cells)):
			y = self.filled_cells[i] // self.num_cols
			x = self.filled_cells[i] % self.num_cols
			rect = [x * self.cell[0] + self.rect[0], y * self.cell[1] + self.rect[1], self.cell[0], self.cell[1]]
			pygame.draw.rect(screen, self.color, rect)

	def update(self, WIDTH):
		if self.rect.x + self.rect.width < 0 or self.rect.x > WIDTH:
			self.rect.x += self.low_speed
		else:
			self.rect.x += self.high_speed
		if self.rect.x > WIDTH + 500:
			self.reset()

	def boom(self, screen):
		self.boomed_rect.x = self.rect.x
		self.boomed_rect.y = self.rect.y
		self.boomed_count += 1
		if self.boomed_count % 1 == 0:
			self.boomed_frame += 1
			for i in range(0, len(self.boomed_filled_cells)):
				y = self.boomed_filled_cells[i] // self.boomed_num_cols
				x = self.boomed_filled_cells[i] % self.boomed_num_cols
				rect = [x * self.boomed_cell[0] + self.boomed_rect[0], y * self.boomed_cell[1] + self.boomed_rect[1], self.boomed_cell[0], self.boomed_cell[1]]
				pygame.draw.rect(screen, self.color, rect)
		if self.boomed_frame > 4:
			return True
		else:
			return False

	def reset(self):
		self.cell = [3, 3]
		self.num_cols = 16
		self.num_rows = 7
		self.rect = pygame.Rect(-500-self.num_cols*self.cell[0], 60, self.num_cols*self.cell[0], self.num_rows*self.cell[1])
		self.filled_cells = [5,6,7,8,9,10,19,20,21,22,23,24,25,26,27,28,34,35,36,37,38,39,40,41,42,43,44,45,49,50,52,53,55,56,58,59,61,62,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,82,83,84,87,88,91,92,93,99,108]
		self.low_speed = 1
		self.high_speed = 2
		self.is_dead = False
		self.has_boomed = False
		self.boomed_filled_cells = [3,7,12,15,17,20,24,30,36,40,44,45,53,54,58,62,68,74,78,81,83,86,91,95]
		self.boomed_cell = [3, 3]
		self.boomed_num_cols = 11
		self.boomed_num_rows = 9
		self.boomed_rect = pygame.Rect(0, 0, self.boomed_num_cols*self.boomed_cell[0], self.boomed_num_rows*self.boomed_cell[1])
		self.boomed_count = 0
		self.boomed_frame = 0

class enemySprite(pygame.sprite.Sprite):
	def __init__(self, category, number, color, bullet_color, **kwargs):
		pygame.sprite.Sprite.__init__(self)
		self.cell = [3, 3]
		self.number = number
		self.category = category
		if category == 'small':
			self.reward = 20
			self.num_cols = 8
			self.num_rows = 8
			self.rect = pygame.Rect(0, 0, self.num_cols*self.cell[0], self.num_rows*self.cell[1])
			self.filled_cells = [[3,4,10,11,12,13,17,18,19,20,21,22,24,25,27,28,30,31,32,33,34,35,36,37,38,39,42,45,49,51,52,54,56,58,61,63], 
								 [3,4,10,11,12,13,17,18,19,20,21,22,24,25,27,28,30,31,32,33,34,35,36,37,38,39,41,43,44,46,48,55,57,62]]
		elif category == 'medium':
			self.reward = 15
			self.num_cols = 11
			self.num_rows = 8
			self.rect = pygame.Rect(0, 0, self.num_cols*self.cell[0], self.num_rows*self.cell[1])
			self.filled_cells = [[2,8,11,14,18,21,22,24,25,26,27,28,29,30,32,33,34,35,37,38,39,41,42,43,44,45,46,47,48,49,50,51,52,53,54,56,57,58,59,60,61,62,63,64,68,74,78,86], 
								 [2,8,14,18,24,25,26,27,28,29,30,34,35,37,38,39,41,42,44,45,46,47,48,49,50,51,52,53,54,55,57,58,59,60,61,62,63,65,66,68,74,76,80,81,83,84]]
		elif category == 'large':
			self.reward = 10
			self.num_cols = 12
			self.num_rows = 8
			self.rect = pygame.Rect(0, 0, self.num_cols*self.cell[0], self.num_rows*self.cell[1])
			self.filled_cells = [[4,5,6,7,13,14,15,16,17,18,19,20,21,22,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,41,42,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,62,63,64,67,68,69,73,74,77,78,81,82,86,87,92,93], 
								 [4,5,6,7,13,14,15,16,17,18,19,20,21,22,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,41,42,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,63,64,67,68,74,75,77,78,80,81,84,85,94,95]]
		self.color = color
		self.bullet_color = bullet_color
		self.speed = [8, 20]
		self.change_count = 0
		self.change_flag = False

		self.boomed_filled_cells = [3,7,12,15,17,20,24,30,36,40,44,45,53,54,58,62,68,74,78,81,83,86,91,95]
		self.boomed_cell = [3, 3]
		self.boomed_num_cols = 11
		self.boomed_num_rows = 9
		self.boomed_rect = pygame.Rect(0, 0, self.boomed_num_cols*self.boomed_cell[0], self.boomed_num_rows*self.boomed_cell[1])
		self.boomed_count = 0
		self.boomed_frame = 0

	def shot(self):
		return enemyBulletSprite(self.rect.x + self.rect.width // 2, self.rect.y, self.bullet_color)

	def draw(self, screen):
		if self.change_count > 50:
			self.change_count = 0
			self.change_flag = not self.change_flag
		if self.change_flag:
			for i in range(0, len(self.filled_cells[0])):
				y = self.filled_cells[0][i] // self.num_cols
				x = self.filled_cells[0][i] % self.num_cols
				rect = [x * self.cell[0] + self.rect[0], y * self.cell[1] + self.rect[1], self.cell[0], self.cell[1]]
				pygame.draw.rect(screen, self.color, rect)
		else:
			for i in range(0, len(self.filled_cells[1])):
				y = self.filled_cells[1][i] // self.num_cols
				x = self.filled_cells[1][i] % self.num_cols
				rect = [x * self.cell[0] + self.rect[0], y * self.cell[1] + self.rect[1], self.cell[0], self.cell[1]]
				pygame.draw.rect(screen, self.color, rect)

	def update(self, direction, HEIGHT):
		self.change_count += 1
		if direction == 'right':
			self.rect.x += self.speed[0]
		elif direction == 'left':
			self.rect.x -= self.speed[0]
		elif direction == 'down':
			self.rect.y += self.speed[1]
		if self.rect.y >= HEIGHT - self.rect.height:
			return True
		else:
			return False

	def boom(self, screen):
		self.boomed_rect.x = self.rect.x
		self.boomed_rect.y = self.rect.y
		self.boomed_count += 1
		if self.boomed_count % 1 == 0:
			self.boomed_frame += 1
			for i in range(0, len(self.boomed_filled_cells)):
				y = self.boomed_filled_cells[i] // self.boomed_num_cols
				x = self.boomed_filled_cells[i] % self.boomed_num_cols
				rect = [x * self.boomed_cell[0] + self.boomed_rect[0], y * self.boomed_cell[1] + self.boomed_rect[1], self.boomed_cell[0], self.boomed_cell[1]]
				pygame.draw.rect(screen, self.color, rect)
		if self.boomed_frame > 4:
			return True
		else:
			return False

class myBulletSprite(pygame.sprite.Sprite):
	def __init__(self, x, y, color, **kwargs):
		pygame.sprite.Sprite.__init__(self)
		self.cell = [2, 2]
		self.num_cols = 1
		self.num_rows = 4
		self.rect = pygame.Rect(x, y, self.num_cols*self.cell[0], self.num_rows*self.cell[1])
		self.filled_cells = [0,1,2,3]
		self.speed = 8
		self.color = color

	def draw(self, screen):
		for i in range(0, len(self.filled_cells)):
			y = self.filled_cells[i] // self.num_cols
			x = self.filled_cells[i] % self.num_cols
			rect = [x * self.cell[0] + self.rect[0], y * self.cell[1] + self.rect[1], self.cell[0], self.cell[1]]
			pygame.draw.rect(screen, self.color, rect)

	def update(self):
		self.rect.y -= self.speed
		if self.rect.y + self.rect.height < 0:
			return True
		else:
			return False

class enemyBulletSprite(pygame.sprite.Sprite):
	def __init__(self, x, y, color):
		pygame.sprite.Sprite.__init__(self)
		self.cell = [3, 3]
		self.num_cols = 3
		self.num_rows = 7
		self.rect = pygame.Rect(x, y, self.num_cols*self.cell[0], self.num_rows*self.cell[1])
		self.filled_cells = [[0,4,8,10,12,16,20], 
							 [2,4,6,10,14,16,18]]
		self.change_count = 0
		self.change_flag = False
		self.speed = 4
		self.color = color

	def draw(self, screen):
		if self.change_count > 2:
			self.change_count = 0
			self.change_flag = not self.change_flag
		if self.change_flag:
			for i in range(0, len(self.filled_cells[0])):
				y = self.filled_cells[0][i] // self.num_cols
				x = self.filled_cells[0][i] % self.num_cols
				rect = [x * self.cell[0] + self.rect[0], y * self.cell[1] + self.rect[1], self.cell[0], self.cell[1]]
				pygame.draw.rect(screen, self.color, rect)
		else:
			for i in range(0, len(self.filled_cells[1])):
				y = self.filled_cells[1][i] // self.num_cols
				x = self.filled_cells[1][i] % self.num_cols
				rect = [x * self.cell[0] + self.rect[0], y * self.cell[1] + self.rect[1], self.cell[0], self.cell[1]]
				pygame.draw.rect(screen, self.color, rect)

	def update(self, HEIGHT):
		self.change_count += 1
		self.rect.y += self.speed
		if self.rect.y > HEIGHT:
			return True
		else:
			return False

def startGame(screen):
	clock = pygame.time.Clock()
	font = pygame.font.SysFont('arial', 18)
	if not os.path.isfile('resources/game_score.txt'):
		f = open('resources/game_score.txt', 'w')
		f.write('0')
		f.close()
	with open('resources/game_score.txt', 'r') as f:
		highest_score = int(f.read().strip())

	enemies_group = pygame.sprite.Group()
	for i in range(55):
		if i < 11:
			enemy = enemySprite('small', i, WHITE, RED)
		elif i < 33:
			enemy = enemySprite('medium', i, WHITE, RED)
		else:
			enemy = enemySprite('large', i, WHITE, RED)
		enemy.rect.x = 85 + (i % 11) * 50
		enemy.rect.y = 120 + (i // 11) * 45
		enemies_group.add(enemy)
	boomed_enemies_group = pygame.sprite.Group()
	en_bullets_group = pygame.sprite.Group()
	ufo = ufoSprite(color=RED)

	myaircraft = aircraftSprite(color=GREEN, bullet_color=WHITE)
	my_bullets_group = pygame.sprite.Group()

	enemy_move_count = 24
	enemy_move_interval = 24
	enemy_move_flag = False

	enemy_change_direction_count = 0
	enemy_change_direction_interval = 60
	enemy_need_down = False
	enemy_move_right = True
	enemy_need_move_row = 6
	enemy_max_row = 5

	enemy_shot_interval = 100
	enemy_shot_count = 0
	enemy_shot_flag = False

	running = True
	is_win = False

	while running:
		screen.fill(BLACK)
		for event in pygame.event.get():
			# 点右上角的X或者按Esc键退出游戏
			if event.type == pygame.QUIT:
				pygame.quit()
				sys.exit()
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_ESCAPE:
					pygame.quit()
					sys.exit()
			# 射击
			if event.type == pygame.MOUSEBUTTONDOWN:
				my_bullet = myaircraft.shot()
				if my_bullet:
					my_bullets_group.add(my_bullet)

		for enemy in enemies_group:
			if pygame.sprite.spritecollide(enemy, my_bullets_group, True, None):
				boomed_enemies_group.add(enemy)
				enemies_group.remove(enemy)
				myaircraft.score += enemy.reward
		if pygame.sprite.spritecollide(ufo, my_bullets_group, True, None):
			ufo.is_dead = True
			myaircraft.score += ufo.reward

		enemy_shot_count += 1
		if enemy_shot_count > enemy_shot_interval:
			enemy_shot_flag = True
			enemies_survive_list = [enemy.number for enemy in enemies_group]
			shot_number = random.choice(enemies_survive_list)
			enemy_shot_count = 0

		enemy_move_count += 1
		if enemy_move_count > enemy_move_interval:
			enemy_move_count = 0
			enemy_move_flag = True
			enemy_need_move_row -= 1
			if enemy_need_move_row == 0:
				enemy_need_move_row = enemy_max_row
			enemy_change_direction_count += 1
			if enemy_change_direction_count > enemy_change_direction_interval:
				enemy_change_direction_count = 1
				enemy_move_right = not enemy_move_right
				enemy_need_down = True

				enemy_move_interval = max(15, enemy_move_interval-3)
				enemy_shot_interval = max(50, enemy_move_interval-10)

		for enemy in enemies_group:
			if enemy_shot_flag:
				if enemy.number == shot_number:
					en_bullet = enemy.shot()
					en_bullets_group.add(en_bullet)
			if enemy_move_flag:
				if enemy.number in range((enemy_need_move_row-1)*11, enemy_need_move_row*11):
					if enemy_move_right:
						enemy.update('right', HEIGHT)
					else:
						enemy.update('left', HEIGHT)
			else:
				enemy.update(None, HEIGHT)
			if enemy_need_down:
				if enemy.update('down', HEIGHT):
					running = False
					is_win = False
				enemy.change_count -= 1
			enemy.draw(screen)
		enemy_move_flag = False
		enemy_need_down = False
		enemy_shot_flag = False

		for boomed_enemy in boomed_enemies_group:
			if boomed_enemy.boom(screen):
				boomed_enemies_group.remove(boomed_enemy)
				del boomed_enemy

		if not myaircraft.one_dead:
			if pygame.sprite.spritecollide(myaircraft, en_bullets_group, True, None):
				myaircraft.one_dead = True
		if myaircraft.one_dead:
			if myaircraft.boom(screen):
				myaircraft.resetBoom()
				myaircraft.num_life -= 1
				if myaircraft.num_life < 1:
					running = False
					is_win = False
		else:
			myaircraft.update(WIDTH)
			myaircraft.draw(screen)
		if (not ufo.has_boomed) and (ufo.is_dead):
			if ufo.boom(screen):
				ufo.has_boomed = True
		else:
			ufo.update(WIDTH)
			ufo.draw(screen)
			
		for bullet in my_bullets_group:
			if bullet.update():
				my_bullets_group.remove(bullet)
				del bullet
			else:
				bullet.draw(screen)

		for bullet in en_bullets_group:
			if bullet.update(HEIGHT):
				en_bullets_group.remove(bullet)
				del bullet
			else:
				bullet.draw(screen)
		if myaircraft.score > highest_score:
			highest_score = myaircraft.score
		# 得分每增加2000我方飞船增加一条生命
		if (myaircraft.score % 2000 == 0) and (myaircraft.score > 0) and (myaircraft.score != myaircraft.old_score):
			myaircraft.old_score = myaircraft.score
			myaircraft.num_life = min(myaircraft.num_life + 1, myaircraft.max_num_life)

		if len(enemies_group) < 1:
			is_win = True
			running = False

		showText(screen, 'SCORE: ', WHITE, font, 200, 8)
		showText(screen, str(myaircraft.score), WHITE, font, 200, 24)

		showText(screen, 'ENEMY: ', WHITE, font, 370, 8)
		showText(screen, str(len(enemies_group)), WHITE, font, 370, 24)

		showText(screen, 'HIGHEST: ', WHITE, font, 540, 8)
		showText(screen, str(highest_score), WHITE, font, 540, 24)

		showText(screen, 'FPS: ' + str(int(clock.get_fps())), RED, font, 8, 8)

		showLife(screen, myaircraft.num_life, GREEN)
		pygame.display.update()
		clock.tick(FPS)
	with open('resources/game_score.txt', 'w') as f:
		f.write(str(highest_score))
	return is_win

def main():
	pygame.init()

	gameIcon = pygame.image.load("./resources/game_icon.jpg")
	pygame.display.set_icon(gameIcon)
	
	pygame.display.set_caption(u'Alien Invasion Game - developed by hanshiqiang365')
	screen = pygame.display.set_mode([WIDTH, HEIGHT])

	pygame.mixer.init()
	pygame.mixer.music.load('resources/game_bgm.mp3')
	pygame.mixer.music.set_volume(0.4)
	pygame.mixer.music.play(-1)

	while True:
		is_win = startGame(screen)
		endInterface(screen, BLACK, is_win)

if __name__ == '__main__':
	main()

