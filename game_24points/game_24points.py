﻿#author: hanshiqiang365 （微信公众号：韩思工作室）

import os
import sys
import copy
import pygame
import random
from fractions import Fraction

RED = (255, 0, 0)
BLACK = (0, 0, 0)
AZURE = (240, 255, 255)
WHITE = (255, 255, 255)
MISTYROSE = (255, 228, 225)
PALETURQUOISE = (175, 238, 238)
PAPAYAWHIP = (255, 239, 213)

BGCOLOR =(120,120,0)

CURRENTPATH = os.path.abspath(os.path.dirname(__file__))

FONTPATH = os.path.join(CURRENTPATH, 'resources/simkai.ttf')
AUDIOWINPATH = os.path.join(CURRENTPATH, 'resources/game_win.wav')
AUDIOLOSEPATH = os.path.join(CURRENTPATH, 'resources/game_lose.wav')
AUDIOWARNPATH = os.path.join(CURRENTPATH, 'resources/game_warn.wav')
BGMPATH = os.path.join(CURRENTPATH, 'resources/game_bgm.mp3')

NUMBERFONT_COLORS = [BLACK, RED]
NUMBERCARD_COLORS = [MISTYROSE, PALETURQUOISE]
NUMBERFONT = [FONTPATH, 50]
NUMBERCARD_POSITIONS = [(25, 50, 150, 200), (225, 50, 150, 200), (425, 50, 150, 200), (625, 50, 150, 200)]

OPREATORS = ['+', '-', '×', '÷']
OPREATORFONT_COLORS = [BLACK, RED]
OPERATORCARD_COLORS = [MISTYROSE, PALETURQUOISE]
OPERATORFONT = [FONTPATH, 30]
OPERATORCARD_POSITIONS = [(230, 300, 50, 50), (330, 300, 50, 50), (430, 300, 50, 50), (530, 300, 50, 50)]

BUTTONS = ['RESET', 'ANSWERS', 'NEXT']
BUTTONFONT_COLORS = [BLACK, BLACK]
BUTTONCARD_COLORS = [MISTYROSE, PALETURQUOISE]
BUTTONFONT = [FONTPATH, 30]
BUTTONCARD_POSITIONS = [(25, 400, 700/3, 150), (50+700/3, 400, 700/3, 150), (75+1400/3, 400, 700/3, 150)]

SCREENSIZE = (800, 600)
GROUPTYPES = ['NUMBER', 'OPREATOR', 'BUTTON']

class Card(pygame.sprite.Sprite):
	def __init__(self, x, y, width, height, text, font, font_colors, bg_colors, attribute, **kwargs):
		pygame.sprite.Sprite.__init__(self)
		self.rect = pygame.Rect(x, y, width, height)
		self.text = text
		self.attribute = attribute
		self.font_info = font
		self.font = pygame.font.Font(font[0], font[1])
		self.font_colors = font_colors
		self.is_selected = False
		self.select_order = None
		self.bg_colors = bg_colors
	
	def draw(self, screen, mouse_pos):
		pygame.draw.rect(screen, self.bg_colors[1], self.rect, 0)
		if self.rect.collidepoint(mouse_pos):
			pygame.draw.rect(screen, self.bg_colors[0], self.rect, 0)
		font_color = self.font_colors[self.is_selected]
		text_render = self.font.render(self.text, True, font_color)
		font_size = self.font.size(self.text)
		screen.blit(text_render, (self.rect.x+(self.rect.width-font_size[0])/2,
								  self.rect.y+(self.rect.height-font_size[1])/2))

class Button(Card):
	def __init__(self, x, y, width, height, text, font, font_colors, bg_colors, attribute, **kwargs):
		Card.__init__(self, x, y, width, height, text, font, font_colors, bg_colors, attribute)

	def do(self, game24_gen, func, sprites_group, objs):
		if self.attribute == 'NEXT':
			for obj in objs:
				obj.font = pygame.font.Font(obj.font_info[0], obj.font_info[1])
				obj.text = obj.attribute
			self.font = pygame.font.Font(self.font_info[0], self.font_info[1])
			self.text = self.attribute
			game24_gen.generate()
			sprites_group = func(game24_gen.numbers_now)
		elif self.attribute == 'RESET':
			for obj in objs:
				obj.font = pygame.font.Font(obj.font_info[0], obj.font_info[1])
				obj.text = obj.attribute
			game24_gen.numbers_now = game24_gen.numbers_ori
			game24_gen.answers_idx = 0
			sprites_group = func(game24_gen.numbers_now)
		elif self.attribute == 'ANSWERS':
			self.font = pygame.font.Font(self.font_info[0], 20)
			self.text = '[%d/%d]: ' % (game24_gen.answers_idx+1, len(game24_gen.answers)) + game24_gen.answers[game24_gen.answers_idx]
			game24_gen.answers_idx = (game24_gen.answers_idx+1) % len(game24_gen.answers)
		else:
			raise ValueError('Button.attribute unsupport <%s>, expect <%s>, <%s> or <%s>...' % (self.attribute, 'NEXT', 'RESET', 'ANSWERS'))
		return sprites_group

class game24Generator():
	def __init__(self):
		self.info = 'game24Generator'

	def generate(self):
		self.__reset()
		while True:
			self.numbers_ori = [random.randint(1, 10) for i in range(4)]
			self.numbers_now = copy.deepcopy(self.numbers_ori)
			self.answers = self.__verify()
			if self.answers:
				break

	def check(self):
		if len(self.numbers_now) == 1 and float(self.numbers_now[0]) == self.target:
			return True
		return False

	def __reset(self):
		self.answers = []
		self.numbers_ori = []
		self.numbers_now = []
		self.target = 24.
		self.answers_idx = 0

	def __verify(self):
		answers = []
		for item in self.__iter(self.numbers_ori, len(self.numbers_ori)):
			item_dict = []
			list(map(lambda i: item_dict.append({str(i): i}), item))
			solution1 = self.__func(self.__func(self.__func(item_dict[0], item_dict[1]), item_dict[2]), item_dict[3])
			solution2 = self.__func(self.__func(item_dict[0], item_dict[1]), self.__func(item_dict[2], item_dict[3]))
			solution = dict()
			solution.update(solution1)
			solution.update(solution2)
			for key, value in solution.items():
				if float(value) == self.target:
					answers.append(key)

		answers = list(set(answers))
		return answers

	def __iter(self, items, n):
		for idx, item in enumerate(items):
			if n == 1:
				yield [item]
			else:
				for each in self.__iter(items[:idx]+items[idx+1:], n-1):
					yield [item] + each

	def __func(self, a, b):
		res = dict()
		for key1, value1 in a.items():
			for key2, value2 in b.items():
				res.update({'('+key1+'+'+key2+')': value1+value2})
				res.update({'('+key1+'-'+key2+')': value1-value2})
				res.update({'('+key2+'-'+key1+')': value2-value1})
				res.update({'('+key1+'×'+key2+')': value1*value2})
				value2 > 0 and res.update({'('+key1+'÷'+key2+')': value1/value2})
				value1 > 0 and res.update({'('+key2+'÷'+key1+')': value2/value1})
		return res

def checkClicked(group, mouse_pos, group_type='NUMBER'):
	selected = []

	if group_type == GROUPTYPES[0] or group_type == GROUPTYPES[1]:
		max_selected = 2 if group_type == GROUPTYPES[0] else 1
		num_selected = 0
		for each in group:
			num_selected += int(each.is_selected)
		for each in group:
			if each.rect.collidepoint(mouse_pos):
				if each.is_selected:
					each.is_selected = not each.is_selected
					num_selected -= 1
					each.select_order = None
				else:
					if num_selected < max_selected:
						each.is_selected = not each.is_selected
						num_selected += 1
						each.select_order = str(num_selected)
			if each.is_selected:
				selected.append(each.attribute)

	elif group_type == GROUPTYPES[2]:
		for each in group:
			if each.rect.collidepoint(mouse_pos):
				each.is_selected = True
				selected.append(each.attribute)
				
	else:
		raise ValueError('checkClicked.group_type unsupport <%s>, expect <%s>, <%s> or <%s>...' % (group_type, *GROUPTYPES))
	return selected

def getNumberSpritesGroup(numbers):
	number_sprites_group = pygame.sprite.Group()
	for idx, number in enumerate(numbers):
		args = (*NUMBERCARD_POSITIONS[idx], str(number), NUMBERFONT, NUMBERFONT_COLORS, NUMBERCARD_COLORS, str(number))
		number_sprites_group.add(Card(*args))
	return number_sprites_group

def getOperatorSpritesGroup(operators):
	operator_sprites_group = pygame.sprite.Group()
	for idx, operator in enumerate(operators):
		args = (*OPERATORCARD_POSITIONS[idx], str(operator), OPERATORFONT, OPREATORFONT_COLORS, OPERATORCARD_COLORS, str(operator))
		operator_sprites_group.add(Card(*args))
	return operator_sprites_group

def getButtonSpritesGroup(buttons):
	button_sprites_group = pygame.sprite.Group()
	for idx, button in enumerate(buttons):
		args = (*BUTTONCARD_POSITIONS[idx], str(button), BUTTONFONT, BUTTONFONT_COLORS, BUTTONCARD_COLORS, str(button))
		button_sprites_group.add(Button(*args))
	return button_sprites_group

def calculate(number1, number2, operator):
	operator_map = {'+': '+', '-': '-', '×': '*', '÷': '/'}
	try:
		result = str(eval(number1+operator_map[operator]+number2))
		return result if '.' not in result else str(Fraction(number1+operator_map[operator]+number2))
	except:
		return None

def showInfo(text, screen):
	rect = pygame.Rect(200, 180, 400, 200)
	pygame.draw.rect(screen, PAPAYAWHIP, rect)
	font = pygame.font.Font(FONTPATH, 40)
	text_render = font.render(text, True, BLACK)
	font_size = font.size(text)
	screen.blit(text_render, (rect.x+(rect.width-font_size[0])/2,
							  rect.y+(rect.height-font_size[1])/2))

def main():
	pygame.init()
	gameIcon = pygame.image.load("./resources/game_icon.jpg")
	pygame.display.set_icon(gameIcon)
	
	screen = pygame.display.set_mode(SCREENSIZE)
	pygame.display.set_caption('24 Points Game - developed by hanshiqiang365')

	pygame.mixer.init()
	win_sound = pygame.mixer.Sound(AUDIOWINPATH)
	lose_sound = pygame.mixer.Sound(AUDIOLOSEPATH)
	warn_sound = pygame.mixer.Sound(AUDIOWARNPATH)
	pygame.mixer.music.load(BGMPATH)
	pygame.mixer.music.play(-1, 0.0)

	screen.fill(BGCOLOR)
	
	game24_gen = game24Generator()
	game24_gen.generate()
	
	number_sprites_group = getNumberSpritesGroup(game24_gen.numbers_now)
	
	operator_sprites_group = getOperatorSpritesGroup(OPREATORS)
	
	button_sprites_group = getButtonSpritesGroup(BUTTONS)
	
	clock = pygame.time.Clock()
	selected_numbers = []
	selected_operators = []
	selected_buttons = []
	is_win = False
	while True:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				sys.exit(-1)
			elif event.type == pygame.MOUSEBUTTONUP:
				mouse_pos = pygame.mouse.get_pos()
				selected_numbers = checkClicked(number_sprites_group, mouse_pos, 'NUMBER')
				selected_operators = checkClicked(operator_sprites_group, mouse_pos, 'OPREATOR')
				selected_buttons = checkClicked(button_sprites_group, mouse_pos, 'BUTTON')
		
		if len(selected_numbers) == 2 and len(selected_operators) == 1:
			noselected_numbers = []
			for each in number_sprites_group:
				if each.is_selected:
					if each.select_order == '1':
						selected_number1 = each.attribute
					elif each.select_order == '2':
						selected_number2 = each.attribute
					else:
						raise ValueError('Unknow select_order <%s>, expect <1> or <2>...' % each.select_order)
				else:
					noselected_numbers.append(each.attribute)
				each.is_selected = False
			for each in operator_sprites_group:
				each.is_selected = False
			result = calculate(selected_number1, selected_number2, *selected_operators)
			if result is not None:
				game24_gen.numbers_now = noselected_numbers + [result]
				is_win = game24_gen.check()
				if is_win:
					win_sound.play()
				if not is_win and len(game24_gen.numbers_now) == 1:
					lose_sound.play()
			else:
				warn_sound.play()
			selected_numbers = []
			selected_operators = []
			number_sprites_group = getNumberSpritesGroup(game24_gen.numbers_now)
		
		for each in number_sprites_group:
			each.draw(screen, pygame.mouse.get_pos())
		for each in operator_sprites_group:
			each.draw(screen, pygame.mouse.get_pos())
		for each in button_sprites_group:
			if selected_buttons and selected_buttons[0] in ['RESET', 'NEXT']:
				is_win = False
			if selected_buttons and each.attribute == selected_buttons[0]:
				each.is_selected = False
				number_sprites_group = each.do(game24_gen, getNumberSpritesGroup, number_sprites_group, button_sprites_group)
				selected_buttons = []
			each.draw(screen, pygame.mouse.get_pos())
		
		if is_win:
			showInfo('Congratulations', screen)
		
		if not is_win and len(game24_gen.numbers_now) == 1:
			showInfo('Game Over', screen)
		pygame.display.flip()
		clock.tick(30)

if __name__ == '__main__':
	main()

